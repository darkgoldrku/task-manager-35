package ru.t1.bugakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;
import ru.t1.bugakov.tm.enumerated.TaskSort;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort taskSort;

    public TaskListRequest(@Nullable TaskSort taskSort) {
        this.taskSort = taskSort;
    }

    public TaskListRequest(@Nullable String token, @Nullable TaskSort taskSort) {
        super(token);
        this.taskSort = taskSort;
    }

}
