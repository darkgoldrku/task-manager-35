package ru.t1.bugakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.task.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        getTaskEndpoint().clearTasks(new TaskClearRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

}
