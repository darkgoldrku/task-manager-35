package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataJsonLoadJaxBRequest;

public final class DataJsonLoadJaxBCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        getDomainEndpoint().JsonLoadJaxB(new DataJsonLoadJaxBRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return "data-load-json-jaxb";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Load data from json file with jaxb";
    }

}
