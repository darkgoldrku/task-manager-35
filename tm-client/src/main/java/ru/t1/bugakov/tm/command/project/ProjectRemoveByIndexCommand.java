package ru.t1.bugakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.project.ProjectRemoveByIndexRequest;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectEndpoint().removeProjectByIndex(new ProjectRemoveByIndexRequest(getToken(), index));
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

}
