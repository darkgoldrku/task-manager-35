package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataBackupLoadRequest;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().BackupLoad(new DataBackupLoadRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return NAME;
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Load backup from BASE64 file";
    }

}
