package ru.t1.bugakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectEndpoint().changeProjectStatusById(new ProjectChangeStatusByIdRequest(getToken(), id, Status.COMPLETED));
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

}
