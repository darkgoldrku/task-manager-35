package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.api.service.IProjectService;
import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.exception.field.DescriptionEmptyException;
import ru.t1.bugakov.tm.exception.field.NameEmptyException;
import ru.t1.bugakov.tm.exception.field.UserIdEmptyException;
import ru.t1.bugakov.tm.model.Project;
import ru.t1.bugakov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private IProjectService projectService;

    @NotNull
    private List<Project> projectList;

    @Before
    public void initService() {
        projectList = new ArrayList<>();
        IProjectRepository projectRepository = new ProjectRepository();
        projectService = new ProjectService(projectRepository);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("TestProject" + i);
            project.setDescription("TestDescription" + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectService.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testCreate() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        projectService.create(USER_ID_1, "TestProjectAdd", "TestDescriptionAdd");
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projectService.getSize());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateEmptyUserId() {
        projectService.create("", "TestProjectAdd", "TestDescriptionAdd");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateEmptyName() {
        projectService.create(USER_ID_1, "", "TestDescriptionAdd");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateEmptyDescription() {
        projectService.create(USER_ID_1, "TestProjectAdd", "");
    }

    @Test
    public void testUpdateById() {
        @NotNull final String newName = "TestProjectUpdate";
        @NotNull final String newDescription = "TestDescriptionUpdate";
        @NotNull final Project projectForUpdate = projectList.get(0);
        projectService.updateById(projectForUpdate.getUserId(), projectForUpdate.getId(), newName, newDescription);
        Assert.assertEquals(newName, projectList.get(0).getName());
        Assert.assertEquals(newDescription, projectList.get(0).getDescription());
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String newName = "TestProjectUpdate";
        @NotNull final String newDescription = "TestDescriptionUpdate";
        @NotNull final Project projectForUpdate = projectList.get(0);
        projectService.updateByIndex(projectForUpdate.getUserId(), 0, newName, newDescription);
        Assert.assertEquals(newName, projectList.get(0).getName());
        Assert.assertEquals(newDescription, projectList.get(0).getDescription());
    }

    @Test
    public void testChangeProjectStatusById() {
        @NotNull final Status newStatus = Status.COMPLETED;
        @NotNull final Project projectForUpdate = projectList.get(0);
        projectService.changeProjectStatusById(projectForUpdate.getUserId(), projectForUpdate.getId(), newStatus);
        Assert.assertEquals(newStatus, projectList.get(0).getStatus());
    }

    @Test
    public void testChangeProjectStatusByIndex() {
        @NotNull final Status newStatus = Status.COMPLETED;
        @NotNull final Project projectForUpdate = projectList.get(0);
        projectService.changeProjectStatusByIndex(projectForUpdate.getUserId(), 0, newStatus);
        Assert.assertEquals(newStatus, projectList.get(0).getStatus());
    }

}
