package ru.t1.bugakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        userRepository = new UserRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("TestUser" + i);
            user.setEmail("Test@User" + i);
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testAdd() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        userRepository.add(new User());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<User> users = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            users.add(new User());
        }
        userRepository.add(users);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        userRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> users = userRepository.findAll();
        Assert.assertEquals(userList, users);
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findById(user.getId()));
            Assert.assertEquals(user, userRepository.findById(user.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(userRepository.findById(id));
    }

    @Test
    public void testFindByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByLogin(user.getLogin()));
            Assert.assertEquals(user, userRepository.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testFindByLoginNegative() {
        @NotNull final String login = "test_login";
        Assert.assertNull(userRepository.findById(login));
    }

    @Test
    public void testFindByEmailPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByEmail(user.getEmail()));
            Assert.assertEquals(user, userRepository.findByEmail(user.getEmail()));
        }
    }

    @Test
    public void testFindByEmailNegative() {
        @NotNull final String email = "test@email";
        Assert.assertNull(userRepository.findByEmail(email));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final User user : userList) {
            userRepository.remove(user);
            Assert.assertNull(userRepository.findById(user.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final User user = new User();
        userRepository.remove(user);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.removeById(user.getId()));
            Assert.assertNull(userRepository.findById(user.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String randomId = UUID.randomUUID().toString();
        Assert.assertNull(userRepository.removeById(randomId));
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final User expected1 = userList.get(0);
        @NotNull final User expected2 = userList.get(userRepository.getSize() - 1);
        Assert.assertEquals(expected1, userRepository.removeByIndex(0));
        Assert.assertEquals(expected2, userRepository.removeByIndex(userRepository.getSize() - 1));
        Assert.assertNull(userRepository.removeByIndex(userRepository.getSize()));
    }

    @Test
    public void testLockUnlockUserByLogin() {
        userRepository.lockUserByLogin(userList.get(0).getLogin());
        Assert.assertTrue(userList.get(0).isLocked());
        userRepository.unlockUserByLogin(userList.get(0).getLogin());
        Assert.assertFalse(userList.get(0).isLocked());
    }

}
