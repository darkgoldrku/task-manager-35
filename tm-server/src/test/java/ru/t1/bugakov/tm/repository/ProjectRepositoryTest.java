package ru.t1.bugakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("TestProject" + i);
            project.setDescription("TestDescription" + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAdd() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project project = new Project();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project actualProject = projectRepository.findByIndex(projectRepository.getSize() - 1);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(userId, actualProject.getUserId());
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Project> projects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            projects.add(new Project());
        }
        projectRepository.add(projects);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        projectRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() {
        @NotNull final List<Project> projects = new ArrayList<>();
        projectRepository.clear(USER_ID_1);
        Assert.assertEquals(projects, projectRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(projects, projectRepository.findAll(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() {
        projectRepository.clear("other_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(projectList, projects);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Project> projects = projectRepository.findAll(USER_ID_1);
        Assert.assertEquals(projectList.subList(0, 5), projects);
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.findById(project.getId()));
            Assert.assertEquals(project, projectRepository.findById(project.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findById(id));
    }

    @Test
    public void testFindByIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(projectRepository.findById(USER_ID_1, project.getId()));
                Assert.assertEquals(project, projectRepository.findById(project.getId()));
            } else Assert.assertNull(projectRepository.findById(USER_ID_1, project.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(projectRepository.getSize() - 1);
        @NotNull final Project expected3 = projectList.get(projectRepository.getSize() / 2);
        Assert.assertEquals(expected1, projectRepository.findByIndex(0));
        Assert.assertEquals(expected2, projectRepository.findByIndex(projectRepository.getSize() - 1));
        Assert.assertEquals(expected3, projectRepository.findByIndex(projectRepository.getSize() / 2));
        Assert.assertNull(projectRepository.findByIndex(projectRepository.getSize()));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(5);
        Assert.assertEquals(expected1, projectRepository.findByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, projectRepository.findByIndex(USER_ID_2, 0));
        Assert.assertNull(projectRepository.findByIndex("test user", 0));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectRepository.getSize(USER_ID_1));
        Assert.assertEquals(0, projectRepository.getSize("test user"));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Project project : projectList) {
            projectRepository.remove(project);
            Assert.assertNull(projectRepository.findById(project.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Project project = new Project();
        projectRepository.remove(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final Project project : projectList) {
            Assert.assertNotNull(projectRepository.removeById(project.getId()));
            Assert.assertNull(projectRepository.findById(project.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String randomId = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.removeById(randomId));
    }

    @Test
    public void testRemoveByIdForUser() {
        for (@NotNull final Project project : projectList) {
            if (project.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(projectRepository.removeById(USER_ID_1, project.getId()));
            } else Assert.assertNull(projectRepository.removeById(USER_ID_1, project.getId()));
        }
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(projectRepository.getSize() - 1);
        Assert.assertEquals(expected1, projectRepository.removeByIndex(0));
        Assert.assertEquals(expected2, projectRepository.removeByIndex(projectRepository.getSize() - 1));
        Assert.assertNull(projectRepository.removeByIndex(projectRepository.getSize()));
    }

    @Test
    public void testRemoveByIndexForUser() {
        @NotNull final Project expected1 = projectList.get(0);
        @NotNull final Project expected2 = projectList.get(5);
        Assert.assertEquals(expected1, projectRepository.removeByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, projectRepository.removeByIndex(USER_ID_2, 0));
        Assert.assertNull(projectRepository.removeByIndex("test user", 0));
    }

}
