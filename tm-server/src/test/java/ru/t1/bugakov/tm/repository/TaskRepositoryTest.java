package ru.t1.bugakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.bugakov.tm.api.repository.ITaskRepository;
import ru.t1.bugakov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("TestTask" + i);
            task.setDescription("TestDescription" + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                task.setProjectId("TestParentProject1");
            } else {
                task.setUserId(USER_ID_2);
                task.setProjectId("TestParentProject2");
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAdd() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Task task = new Task();
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Description";
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task actualTask = taskRepository.findByIndex(taskRepository.getSize() - 1);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(userId, actualTask.getUserId());
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
    }

    @Test
    public void testAddAll() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES * 2;
        @NotNull List<Task> tasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            tasks.add(new Task());
        }
        taskRepository.add(tasks);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        taskRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClearForUserPositive() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        taskRepository.clear(USER_ID_1);
        Assert.assertEquals(tasks, taskRepository.findAll(USER_ID_1));
        Assert.assertNotEquals(tasks, taskRepository.findAll(USER_ID_2));
    }

    @Test
    public void testClearForUserNegative() {
        taskRepository.clear("other_id");
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> tasks = taskRepository.findAll(USER_ID_1);
        Assert.assertEquals(taskList.subList(0, 5), tasks);
    }

    @Test
    public void testFindByIdPositive() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.findById(task.getId()));
            Assert.assertEquals(task, taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void testFindByIdNegative() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findById(id));
    }

    @Test
    public void testFindByIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(taskRepository.findById(USER_ID_1, task.getId()));
                Assert.assertEquals(task, taskRepository.findById(task.getId()));
            } else Assert.assertNull(taskRepository.findById(USER_ID_1, task.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(taskRepository.getSize() - 1);
        @NotNull final Task expected3 = taskList.get(taskRepository.getSize() / 2);
        Assert.assertEquals(expected1, taskRepository.findByIndex(0));
        Assert.assertEquals(expected2, taskRepository.findByIndex(taskRepository.getSize() - 1));
        Assert.assertEquals(expected3, taskRepository.findByIndex(taskRepository.getSize() / 2));
        Assert.assertNull(taskRepository.findByIndex(taskRepository.getSize()));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(5);
        Assert.assertEquals(expected1, taskRepository.findByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, taskRepository.findByIndex(USER_ID_2, 0));
        Assert.assertNull(taskRepository.findByIndex("test user", 0));
    }

    @Test
    public void testFindAllByProjectIdPositive() {
        Assert.assertEquals(taskRepository.findAllByProjectId(USER_ID_1, "TestParentProject1"), taskRepository.findAll(USER_ID_1));
    }

    @Test
    public void testFindAllByProjectIdNegative() {
        Assert.assertNotEquals(taskRepository.findAllByProjectId(USER_ID_1, "TestParentProject2"), taskRepository.findAll(USER_ID_1));
    }

    @Test
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(0, taskRepository.getSize("test user"));
    }

    @Test
    public void testRemovePositive() {
        for (@NotNull final Task task : taskList) {
            taskRepository.remove(task);
            Assert.assertNull(taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void testRemoveNegative() {
        @NotNull final Task task = new Task();
        taskRepository.remove(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIdPositive() {
        for (@NotNull final Task task : taskList) {
            Assert.assertNotNull(taskRepository.removeById(task.getId()));
            Assert.assertNull(taskRepository.findById(task.getId()));
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        @NotNull final String randomId = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.removeById(randomId));
    }

    @Test
    public void testRemoveByIdForUser() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                Assert.assertNotNull(taskRepository.removeById(USER_ID_1, task.getId()));
            } else Assert.assertNull(taskRepository.removeById(USER_ID_1, task.getId()));
        }
    }

    @Test
    public void testRemoveByIndex() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(taskRepository.getSize() - 1);
        Assert.assertEquals(expected1, taskRepository.removeByIndex(0));
        Assert.assertEquals(expected2, taskRepository.removeByIndex(taskRepository.getSize() - 1));
        Assert.assertNull(taskRepository.removeByIndex(taskRepository.getSize()));
    }

    @Test
    public void testRemoveByIndexForUser() {
        @NotNull final Task expected1 = taskList.get(0);
        @NotNull final Task expected2 = taskList.get(5);
        Assert.assertEquals(expected1, taskRepository.removeByIndex(USER_ID_1, 0));
        Assert.assertEquals(expected2, taskRepository.removeByIndex(USER_ID_2, 0));
        Assert.assertNull(taskRepository.removeByIndex("test user", 0));
    }

}
