package ru.t1.bugakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.api.repository.ITaskRepository;
import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.api.service.IPropertyService;
import ru.t1.bugakov.tm.api.service.IUserService;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.exception.entity.UserNotFoundException;
import ru.t1.bugakov.tm.exception.field.EmailEmptyException;
import ru.t1.bugakov.tm.exception.field.LoginEmptyException;
import ru.t1.bugakov.tm.exception.field.PasswordEmptyException;
import ru.t1.bugakov.tm.model.User;
import ru.t1.bugakov.tm.repository.ProjectRepository;
import ru.t1.bugakov.tm.repository.TaskRepository;
import ru.t1.bugakov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserServiceTest {

    @NotNull
    private IUserService userService;

    @NotNull
    private List<User> userList;

    @Before
    public void initService() {
        userList = new ArrayList<>();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);
        @NotNull final User admin = new User("admin", "admin", "admin@admin");
        @NotNull final User user = new User("user", "user", "user@user");
        @NotNull final User cat = new User("cat", "cat", "cat@cat");
        @NotNull final User mouse = new User("mouse", "mouse", "mouse@mouse");
        userList.add(admin);
        userList.add(user);
        userList.add(cat);
        userList.add(mouse);
        userService.add(admin);
        userService.add(user);
        userService.add(cat);
        userService.add(mouse);
    }

    @Test
    public void testCreate() {
        Assert.assertEquals(4, userService.getSize());
        userService.create("dog", "dog");
        Assert.assertEquals(5, userService.getSize());
        userService.create("fox", "fox", "fox@fox");
        Assert.assertEquals(6, userService.getSize());
        userService.create("lion", "lion", Role.ADMIN);
        Assert.assertEquals(7, userService.getSize());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateEmptyLogin() {
        userService.create("", "dog");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreateEmptyPassword() {
        userService.create("dog", "");
    }

    @Test(expected = EmailEmptyException.class)
    public void testCreateEmptyEmail() {
        userService.create("dog", "dog", "");
    }

    @Test
    public void testFindByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userService.findByLogin(user.getLogin()));
            Assert.assertEquals(user, userService.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testFindByLoginNegative() {
        @NotNull final String login = "test_login";
        Assert.assertNull(userService.findById(login));
    }

    @Test
    public void testFindByEmailPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userService.findByEmail(user.getEmail()));
            Assert.assertEquals(user, userService.findByEmail(user.getEmail()));
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testFindByEmailNegative() {
        @NotNull final String email = "test@email";
        Assert.assertNull(userService.findByEmail(email));
    }

    @Test
    public void testRemoveByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userService.removeByLogin(user.getLogin()));
            Assert.assertNull(userService.findById(user.getId()));
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveByLoginNegative() {
        @NotNull final String randomId = UUID.randomUUID().toString();
        Assert.assertNull(userService.removeByLogin(randomId));
    }

    @Test
    public void testRemoveByEmailPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userService.removeByEmail(user.getEmail()));
            Assert.assertNull(userService.findById(user.getId()));
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveByEmailNegative() {
        @NotNull final String randomId = UUID.randomUUID().toString();
        Assert.assertNull(userService.removeByEmail(randomId));
    }

    @Test
    public void testSetPassword() {
        @NotNull final String newPassword = "newPassword";
        @NotNull final String oldPassword = userList.get(0).getPasswordHash();
        userService.setPassword(userList.get(0).getId(), newPassword);
        Assert.assertNotEquals(oldPassword, userList.get(0).getPasswordHash());
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String newFirstName = "testFirstName";
        @NotNull final String newLastName = "testLastName";
        @NotNull final String newMiddleName = "testMiddleName";
        userService.updateUser(userList.get(0).getId(), newFirstName, newLastName, newMiddleName);
        Assert.assertEquals(newFirstName, userList.get(0).getFirstName());
        Assert.assertEquals(newLastName, userList.get(0).getLastName());
        Assert.assertEquals(newMiddleName, userList.get(0).getMiddleName());
    }

    @Test
    public void testLockUnlockUserByLogin() {
        userService.lockUserByLogin(userList.get(0).getLogin());
        Assert.assertTrue(userList.get(0).isLocked());
        userService.unlockUserByLogin(userList.get(0).getLogin());
        Assert.assertFalse(userList.get(0).isLocked());
    }

}
