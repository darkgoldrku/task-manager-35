package ru.t1.bugakov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.endpoint.IAbstractEndpoint;
import ru.t1.bugakov.tm.api.service.IServiceLocator;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.exception.user.AccessDeniedException;
import ru.t1.bugakov.tm.exception.user.PermissionException;
import ru.t1.bugakov.tm.model.Session;


abstract class AbstractEndpoint implements IAbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }


    protected Session check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final Session session = serviceLocator.getAuthService().validateToken(token);
        if (session.getRole() == null) throw new PermissionException();
        if (!session.getRole().equals(role)) throw new PermissionException();
        return session;
    }

    protected Session check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

}