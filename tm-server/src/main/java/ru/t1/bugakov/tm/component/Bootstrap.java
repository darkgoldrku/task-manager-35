package ru.t1.bugakov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.api.endpoint.*;
import ru.t1.bugakov.tm.api.repository.IProjectRepository;
import ru.t1.bugakov.tm.api.repository.ISessionRepository;
import ru.t1.bugakov.tm.api.repository.ITaskRepository;
import ru.t1.bugakov.tm.api.repository.IUserRepository;
import ru.t1.bugakov.tm.api.service.*;
import ru.t1.bugakov.tm.endpoint.*;
import ru.t1.bugakov.tm.enumerated.Role;
import ru.t1.bugakov.tm.model.User;
import ru.t1.bugakov.tm.repository.ProjectRepository;
import ru.t1.bugakov.tm.repository.SessionRepository;
import ru.t1.bugakov.tm.repository.TaskRepository;
import ru.t1.bugakov.tm.repository.UserRepository;
import ru.t1.bugakov.tm.service.*;
import ru.t1.bugakov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    {
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = this.getPropertyService().getServerHost();
        @NotNull final String port = this.getPropertyService().getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }


    private void initDemoData() {
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        projectService.create(admin.getId(), "p111", "222");
        taskService.create(admin.getId(), "t001", "222");
        taskService.create(admin.getId(), "t002", "444");

        @NotNull final User test = userService.create("test", "test", "test@tast.ru");
        projectService.create(test.getId(), "p333", "444");
        taskService.create(test.getId(), "t003", "222");
        taskService.create(test.getId(), "t004", "444");

        userService.create("user", "user", "user@user.ru");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start() {
        initPID();
        initDemoData();
        backup.start();
        loggerService.info("** WELCOME TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        loggerService.info("TASK MANAGER IS SHUTTING DOWN");
        backup.stop();
    }

}